package pl.utp.edu.makzyt.ant_ai;

import org.junit.Assert;
import org.junit.Test;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Node;
import pl.utp.edu.makzyt.ant_ai.util.ConfigUtils;

import java.awt.*;

public class ConfigUtilsTests {

  @Test
  public void testGettingConfigPrefix() {
    Node node = new Node(new Point(5, 5), Node.Type.NODE_VISIT_POINT);

    String configPrefix = ConfigUtils.getConfigPrefix(node.getType().name());

    Assert.assertEquals("nodeVisitPoint", configPrefix);
  }

  @Test
  public void testGettingConfigFullName() {
    Node node = new Node(new Point(5, 5), Node.Type.NODE_VISIT_POINT);

    String configPrefix
      = ConfigUtils.getConfigPrefix(node.getType().name() + "_Color");

    Assert.assertEquals("nodeVisitPointColor", configPrefix);
  }
}
