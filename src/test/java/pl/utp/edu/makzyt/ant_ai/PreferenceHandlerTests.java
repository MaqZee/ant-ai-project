package pl.utp.edu.makzyt.ant_ai;

import org.junit.Assert;
import org.junit.Test;
import pl.utp.edu.makzyt.ant_ai.util.JsonPreferences;

import java.io.File;
import java.io.IOException;

public class PreferenceHandlerTests {

  private static final String resourceDir
    = "src" + File.separator
    + "test" + File.separator
    + "resources" + File.separator;

  @Test
  public void testOpeningNonExistingFile()
    throws IOException {

    JsonPreferences prefs = new JsonPreferences(resourceDir + "testONEF.json");
    prefs.load();

    Assert.assertEquals(1.0, (double) prefs.get("minPheromoneLevel"), 0.001);
  }

  @Test
  public void testOpeningExistingFile()
    throws IOException {

    JsonPreferences prefs1 = new JsonPreferences(resourceDir + "testOEF.json");
    prefs1.load();
    prefs1.put("minPheromoneLevel", 2.5);
    prefs1.save();

    JsonPreferences prefs2 = new JsonPreferences(resourceDir + "testOEF.json");
    prefs2.load();

    Assert.assertEquals(2.5, (double) prefs2.get("minPheromoneLevel"), 0.001);
  }

  @Test
  public void testPrefsPath() {
    Assert.assertEquals("/home/maqzee/.ant_ai/prefs.json", JsonPreferences.PREFS_PATH);
  }
}
