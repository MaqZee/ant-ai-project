package pl.utp.edu.makzyt.ant_ai;

import org.junit.Assert;
import org.junit.Test;
import pl.utp.edu.makzyt.ant_ai.util.MathUtils;

public class MathUtilsTests {

  @Test
  public void testGreater() {
    Assert.assertEquals(5, MathUtils.ensureRange(7, 2, 5));
  }

  @Test
  public void testLesser() {
    Assert.assertEquals(2, MathUtils.ensureRange(-1, 2, 5));
  }

  @Test
  public void testInRange() {
    Assert.assertEquals(4, MathUtils.ensureRange(4, 2, 5));
  }
}
