package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.MenuFactory;
import pl.utp.edu.makzyt.ant_ai.ui.menus.context.AppContextMenu;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
public final class NodeCanvas
  extends Pane {

  private static final Color backgroundColor
    = Color.web(MainWindow.config.valueOf("canvasBackgroundColor"));

  private final Canvas view = new Canvas();
  private final GraphicsContext gc = view.getGraphicsContext2D();
  private final List<Ant> ants;
  private final InfoText infoText = new InfoText();
  private final PhantomLine phantomLine = new PhantomLine(new Position(0, 0), new Position(0, 0));
  private List<NodeConnection> connections;
  private List<List<Node>> nodes;
  private AppContextMenu contextMenu;
  private SimulationStatus status = SimulationStatus.STOPPED;

  public NodeCanvas() {
    this(Integer.parseInt(MainWindow.config.valueOf("defaultNodeAmountX")),
      Integer.parseInt(MainWindow.config.valueOf("defaultNodeAmountY")));
  }

  public NodeCanvas(int w, int h) {
    initGraphicsContext();

    getChildren().add(view);
    nodes = nodeGrid(w, h);
    connections = new ArrayList<>();
    ants = new ArrayList<>();

    view.addEventHandler(MouseEvent.MOUSE_CLICKED,
      t -> EventHandlingMethods.handleMouseClicked(this, t));

    view.addEventHandler(MouseEvent.MOUSE_MOVED,
      t -> EventHandlingMethods.handleMouseMoved(this, t));

    addEventHandler(MouseEvent.MOUSE_PRESSED,
      t -> hideContextMenu());

    runAnimationTimer();
  }

  private void runAnimationTimer() {
    new AnimationTimer() {
      @Override
      public void handle(long l) {
        draw();

        if (status == SimulationStatus.RUNNING) {
          long readyAnts = ants.stream().filter(Ant::isRunning).count();

          if (readyAnts == 0) {
            for (NodeConnection nc : connections)
              nc.evaporatePheromone();
          }

          for (Ant ant : ants) {
            if (readyAnts == 0) {
              ant.setRunning(true);
            }

            if (ant.isRunning())
              ant.move();
          }
        }
      }
    }.start();
  }

  private void initGraphicsContext() {
    gc.setLineWidth(Integer.parseInt(MainWindow.config.valueOf("connectionThickness")));
  }

  @Override
  protected void layoutChildren() {
    final int top = (int) snappedTopInset();
    final int right = (int) snappedRightInset();
    final int bottom = (int) snappedBottomInset();
    final int left = (int) snappedLeftInset();

    final int width = (int) getWidth() - left - right;
    final int height = (int) getHeight() - top - bottom;

    view.setLayoutX(left);
    view.setLayoutY(top);

    if (width != view.getWidth() || height != view.getHeight()) {
      view.setWidth(width);
      view.setHeight(height);

      draw();
    }
  }

  private List<List<Node>> nodeGrid(int x, int y) {
    List<List<Node>> nodes = new ArrayList<>();
    for (int i = 0; i < y; i++) {
      List<Node> nodeLine = new ArrayList<>();
      for (int j = 0; j < x; j++) {
        nodeLine.add(new Node(new Position(j, i), Node.Type.NODE_INACTIVE));
      }
      nodes.add(nodeLine);
    }

    return nodes;
  }

  public void makeNewGrid(int w, int h) {
    nodes.clear();
    connections.clear();
    nodes = nodeGrid(w, h);
  }

  public void draw() {
    gc.setFill(backgroundColor);
    gc.fillRect(getLayoutX(), getLayoutY(), getWidth(), getHeight());

    for (NodeConnection nc : connections) {
      nc.draw(gc);
    }

    if (phantomLine.isActive())
      phantomLine.draw(gc);

    for (int y = 0; y < nodes.size(); y++) {
      for (int x = 0; x < nodes.get(y).size(); x++) {
        nodes.get(y).get(x).draw(gc);
      }
    }

    for (Ant ant : ants) {
      ant.draw(gc);
    }

    infoText.draw(gc);
  }

  public void hideContextMenu() {
    if (contextMenu != null)
      contextMenu.hide();
  }

  public void toggleSimulation() {
    if (status == SimulationStatus.PAUSED || status == SimulationStatus.STOPPED) {
      startSimulation();
    } else
      pauseSimulation();
  }

  public void startSimulation() {
    if (status == SimulationStatus.STOPPED) {
      for (int y = 0; y < nodes.size(); y++) {
        for (int x = 0; x < nodes.get(y).size(); x++) {
          Node node = nodes.get(y).get(x);

          if (node.getType() == Node.Type.NODE_SPAWNER)
            spawnAnts(node, 5);
        }
      }
    }

    status = SimulationStatus.RUNNING;
    MainWindow.controlPane.getPlayButton().setText(MenuFactory.bundle.valueOf("button.pause"));
  }

  public void pauseSimulation() {
    status = SimulationStatus.PAUSED;
    MainWindow.controlPane.getPlayButton().setText(MenuFactory.bundle.valueOf("button.play"));
  }

  public void stopSimulation() {
    status = SimulationStatus.STOPPED;

    ants.clear();
    Ant.shortestPath.clear();

    for (NodeConnection nc : connections)
      nc.setPheromoneLevel((double) MainWindow.prefs.get("minPheromoneLevel"));

    MainWindow.controlPane.getPlayButton().setText(MenuFactory.bundle.valueOf("button.play"));
  }

  public void spawnAnts(Node node, int amount) {
    assert amount >= 0;

    for (int i = 0; i < amount; i++) {
      Ant ant = new Ant(node, this);
      ants.add(ant);
    }
  }

  public long amountOfVisitPoints() {
    final List<Node> allNodes = nodes.stream().flatMap(List::stream).collect(Collectors.toList());
    final long amount = allNodes.stream().filter(n -> n.getType() == Node.Type.NODE_VISIT_POINT).count();

    return amount;
  }

  public enum SimulationStatus {
    RUNNING,
    PAUSED,
    STOPPED
  }
}
