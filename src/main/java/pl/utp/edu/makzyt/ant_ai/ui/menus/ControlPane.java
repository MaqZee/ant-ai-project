package pl.utp.edu.makzyt.ant_ai.ui.menus;

import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.MenuFactory;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

@Data
public final class ControlPane extends VBox {

  private final LabeledConfigSlider minPheromoneLevelSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.minPheromoneLevel"),
    new Slider(0.1, 1, (double) MainWindow.prefs.get("minPheromoneLevel")), "minPheromoneLevel");
  private final LabeledConfigSlider maxPheromoneLevelSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.maxPheromoneLevel"),
    new Slider(0.1, 10, (double) MainWindow.prefs.get("maxPheromoneLevel")), "maxPheromoneLevel");
  private final LabeledConfigSlider evaporationSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.evaporation"),
    new Slider(0.1, 1, (double) MainWindow.prefs.get("evaporationFactor")), "evaporationFactor");
  private final LabeledConfigSlider pheromoneDepositSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.deposit"),
    new Slider(0.1, 1, (double) MainWindow.prefs.get("depositQuantity")), "depositQuantity");
  private final LabeledConfigSlider antSpeedSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.speed"),
    new Slider(0.1, 50, (double) MainWindow.prefs.get("antSpeed")), "antSpeed");
  private final LabeledConfigSlider pheromoneFactorSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.pheromoneFactor"),
    new Slider(0, 1, (double) MainWindow.prefs.get("slider.pheromoneFactor")), "slider.pheromoneFactor");
  private final LabeledConfigSlider heuristicFactorSlider = new LabeledConfigSlider(
    MenuFactory.bundle.valueOf("slider.heuristicFactor"),
    new Slider(0, 1, (double) MainWindow.prefs.get("slider.heuristicFactor")), "slider.heuristicFactor");
  private final Button playButton;
  private final Button stopButton;
  private final LabeledCheckBox inactiveNodesCheckBox;
  private final LabeledCheckBox shortestPathCheckBox;

  public ControlPane() {
    super(25);

    setMinWidth(200);
    setMaxWidth(280);
    setPadding(new Insets(10));

    playButton = new Button(MenuFactory.bundle.valueOf("button.play"));
    playButton.setOnAction(e -> MainWindow.canvas.toggleSimulation());
    playButton.setMinWidth(50);
    playButton.setMaxWidth(50);

    stopButton = new Button(MenuFactory.bundle.valueOf("button.stop"));
    stopButton.setOnAction(e -> MainWindow.canvas.stopSimulation());
    stopButton.setMinWidth(50);
    stopButton.setMaxWidth(50);

    HBox simulationButtons = new HBox(playButton, stopButton);

    inactiveNodesCheckBox = new LabeledCheckBox(MenuFactory.bundle.valueOf("checkbox.inactive"));
    inactiveNodesCheckBox.getBox().setOnAction(e -> MainWindow.canvas.draw());
    inactiveNodesCheckBox.getBox().setSelected(true);

    shortestPathCheckBox = new LabeledCheckBox(MenuFactory.bundle.valueOf("checkbox.shortest"));
    shortestPathCheckBox.getBox().setOnAction(e -> MainWindow.canvas.draw());
    shortestPathCheckBox.getBox().setSelected(true);

    getChildren().addAll(minPheromoneLevelSlider, maxPheromoneLevelSlider,
      evaporationSlider, pheromoneDepositSlider, pheromoneFactorSlider, heuristicFactorSlider,
      new Separator(), simulationButtons, new Separator(), antSpeedSlider, inactiveNodesCheckBox, shortestPathCheckBox);
  }
}
