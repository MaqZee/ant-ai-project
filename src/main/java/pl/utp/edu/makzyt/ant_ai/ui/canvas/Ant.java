package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.transform.Rotate;
import lombok.Data;
import lombok.NonNull;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;
import pl.utp.edu.makzyt.ant_ai.util.MathUtils;

import java.util.*;
import java.util.stream.Collectors;

@Data
public final class Ant
  implements Drawable {

  public static List<NodeConnection> shortestPath = new ArrayList<>();
  @NonNull
  private final Node startNode;
  private final NodeCanvas canvas;
  private final AntShape shape;
  private final Stack<NodeConnection> trail;
  private final Set<Node> visitedPoints;
  private double angle = 0;
  private double cost = 0;
  private boolean goingForward = true;
  private boolean running = true;
  private Position targetPos;
  private Node targetNode;
  private NodeConnection currentConnection;

  public Ant(Node startNode, NodeCanvas canvas) {
    this.canvas = canvas;
    this.startNode = startNode;
    shape = new AntShape(new Position(startNode.relativeCenter()));
    trail = new Stack<>();
    visitedPoints = new HashSet<>();
    setTarget(startNode);
  }

  public void move() {
    if (reachedDestination()) {
      if (currentConnection != null) {
        if (goingForward) {
          cost += currentConnection.length();
          trail.push(currentConnection);
        } else if (cost != 0)
          depositPheromone();
      }

      takeActionOnVisit();

      if (running) {
        if (goingForward)
          findNewTarget();
        else
          returnBack();
      }
    }

    moveTowardTarget();
  }

  private void returnBack() {
    if (targetNode != null) {
      currentConnection = trail.pop();
      setTarget(currentConnection.getPartnerOfNode(targetNode));
    } else {
      setTarget(startNode);
    }
  }

  private double speed() {
    return (double) MainWindow.prefs.get("antSpeed");
  }

  private void takeActionOnVisit() {
    if (targetNode != null) {
      switch (targetNode.getType()) {
        case NODE_SPAWNER:
          visitedPoints.clear();
          cost = 0;
          currentConnection = null;
          trail.clear();

          if (!goingForward) {
            running = false;
          }

          goingForward = true;
          break;
        case NODE_VISIT_POINT:
          visitedPoints.add(targetNode);

          if (visitedPoints.size() == canvas.amountOfVisitPoints() && goingForward) {
            replacePathIfBetter();
            goingForward = false;
          }
          break;
      }
    }
  }

  private void findNewTarget() {
    final List<NodeConnection> connections = availableConnections();
    final List<NodeConnection> heuristicConnections = connections
      .stream()
      .filter(nc -> shortestPath.contains(nc))
      .collect(Collectors.toList());

    if (!connections.isEmpty()) {
      if (heuristicConnections.size() != 0) {
        final double alpha = (double) MainWindow.prefs.get("slider.pheromoneFactor");
        final double beta = (double) MainWindow.prefs.get("slider.heuristicFactor");
        final double random = MathUtils.randomDouble(0, alpha + beta);

        if (random < alpha) {
          takeRoute(heuristicConnections);
          return;
        }
      }
      takeRoute(connections);
    } else {
      if (currentConnection != null)
        currentConnection = null;
      if (startNode != null)
        setTarget(startNode);
    }
  }

  private void takeRoute(List<NodeConnection> connections) {
    currentConnection = pickConnectionByPheromones(connections);

    if (currentConnection == null)
      setTarget(startNode);
    else
      setTarget(currentConnection.getPartnerOfNode(targetNode));
  }

  private void replacePathIfBetter() {
    final List<NodeConnection> path = new ArrayList<>(trail);

    if (shortestPath.isEmpty())
      shortestPath = path;

    final double sum = path.stream().mapToDouble(NodeConnection::length).sum();
    final double sumSoFar = shortestPath.stream().mapToDouble(NodeConnection::length).sum();

    if (sum < sumSoFar)
      shortestPath = path;
  }

  private List<NodeConnection> availableConnections() {
    return (currentConnection != null)
      ? targetNode.connections(canvas).stream().filter(nc -> nc != currentConnection).collect(Collectors.toList())
      : targetNode.connections(canvas);
  }

  private NodeConnection pickConnectionByPheromones(List<NodeConnection> connections) {
    final double sum = connections.stream().mapToDouble(NodeConnection::getPheromoneLevel).sum();
    final double random = MathUtils.randomDouble(0, sum);
    double iter = 0;

    for (NodeConnection nc : connections) {
      iter += nc.getPheromoneLevel();

      if (iter >= random)
        return nc;
    }

    return null;
  }

  public void rotate(GraphicsContext gc) {
    final Rotate r = new Rotate(angle, shape.getPosition().x, shape.getPosition().y);
    gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
  }

  private double depositQuantity() {
    return (double) MainWindow.prefs.get("depositQuantity");
  }

  public void moveTowardTarget() {
    final double dx = targetPos.x - shape.getPosition().x;
    final double dy = targetPos.y - shape.getPosition().y;

    final double length = Math.sqrt(dx * dx + dy * dy);

    shape.getPosition().x += (dx / length) * speed();
    shape.getPosition().y += (dy / length) * speed();
  }

  private void depositPheromone() {
    currentConnection.setPheromoneLevel(currentConnection.getPheromoneLevel() + depositQuantity() / cost);
  }

  private void rotateTowardDestination(Position position) {
    final double dx = shape.getPosition().x - position.x;
    final double dy = shape.getPosition().y - position.y;

    if (dx == 0) {
      angle = (dy > 0) ? 0 : 180;
    } else {
      angle = (dx > 0) ? Math.toDegrees(Math.atan(dy / dx)) - 90 : Math.toDegrees(Math.atan(dy / dx)) + 90;
    }
  }

  private boolean reachedDestination() {
    return (Math.abs(shape.getPosition().x - targetPos.x) < speed())
      && (Math.abs(shape.getPosition().y - targetPos.y) < speed());
  }

  public void setTarget(Node node) {
    targetPos = new Position(node.relativeCenter());
    targetNode = node;
    rotateTowardDestination(targetPos);
  }

  @Override
  public void draw(GraphicsContext gc) {
    gc.setFill(Color.BLACK);
    gc.save();
    rotate(gc);
    shape.draw(gc);
    gc.restore();
  }
}
