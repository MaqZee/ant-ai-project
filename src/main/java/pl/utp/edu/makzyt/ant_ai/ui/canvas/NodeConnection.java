package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;
import pl.utp.edu.makzyt.ant_ai.util.MathUtils;

import java.io.Serializable;


@Data
@NoArgsConstructor
public final class NodeConnection
  implements Drawable, Serializable {

  private static final Color minLevelColor = Color.web(MainWindow.config.valueOf("minLevelColor"));
  private static final Color maxLevelColor = Color.web(MainWindow.config.valueOf("maxLevelColor"));
  private static final int THICKNESS = Integer.parseInt(MainWindow.config.valueOf("connectionThickness"));

  private ConnectionEntry<Node, Node> entry;
  private boolean selected = false;
  private double pheromoneLevel = (double) MainWindow.prefs.get("minPheromoneLevel");

  public NodeConnection(Node node1, Node node2) {

    entry = (node1.getPosition().x < node2.getPosition().x)
      ? new ConnectionEntry<>(node1, node2)
      : new ConnectionEntry<>(node2, node1);
  }

  public void evaporatePheromone() {
    pheromoneLevel -= (double) MainWindow.prefs.get("evaporationFactor");
    pheromoneLevel = MathUtils.ensureRange(pheromoneLevel,
      (double) MainWindow.prefs.get("minPheromoneLevel"),
      (double) MainWindow.prefs.get("maxPheromoneLevel"));
  }

  public void setPheromoneLevel(double ph) {
    pheromoneLevel = ph;
    pheromoneLevel = MathUtils.ensureRange(pheromoneLevel,
      (double) MainWindow.prefs.get("minPheromoneLevel"),
      (double) MainWindow.prefs.get("maxPheromoneLevel"));
  }

  public double length() {
    final Position p1 = entry.x.getPosition();
    final Position p2 = entry.y.getPosition();

    final double dx = Math.abs(p1.x - p2.x);
    final double dy = Math.abs(p1.y - p2.y);

    return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
  }

  @Override
  public void draw(GraphicsContext gc) {
    final Position p1 = entry.x.relativePosition();
    final Position p2 = entry.y.relativePosition();

    final double r1 = entry.x.radius() / 2;
    final double r2 = entry.y.radius() / 2;

    int lineThickness = THICKNESS;

    if (selected)
      lineThickness += 2;

    gc.setLineWidth(lineThickness);

    if (Ant.shortestPath.contains(this) && MainWindow.controlPane.getShortestPathCheckBox().getBox().isSelected())
      gc.setStroke(Color.PURPLE);
    else
      gc.setStroke(color());

    gc.strokeLine(p1.x + r1, p1.y + r1, p2.x + r2, p2.y + r2);
  }

  private Color color() {
    final double factor = (pheromoneLevel - (double) MainWindow.prefs.get("minPheromoneLevel"))
      / ((double) MainWindow.prefs.get("maxPheromoneLevel")
      - (double) MainWindow.prefs.get("minPheromoneLevel"));

    final double red = minLevelColor.getRed() + (maxLevelColor.getRed() - minLevelColor.getRed()) * factor;
    final double green = minLevelColor.getGreen() + (maxLevelColor.getGreen() - minLevelColor.getGreen()) * factor;
    final double blue = minLevelColor.getBlue() + (maxLevelColor.getBlue() - minLevelColor.getBlue()) * factor;

    Color color = new Color(
      MathUtils.ensureRange(red, 0, 1),
      MathUtils.ensureRange(green, 0, 1),
      MathUtils.ensureRange(blue, 0, 1),
      1);

    if (selected)
      color = ColorManipulator.brighten(color, 0.05);

    return color;
  }

  public boolean inArea(Position point) {
    final Position p1 = entry.x.relativeCenter();
    final Position p2 = entry.y.relativeCenter();

    final int h = THICKNESS;

    final double xmin = Math.min(p1.x, p2.x);
    final double xmax = Math.max(p1.x, p2.x);

    final double ymin = Math.min(p1.y, p2.y);
    final double ymax = Math.max(p1.y, p2.y);

    if ((point.x < (xmin - (h / 2))) || (point.x > (xmax + (h / 2)))
      || (point.y < (ymin - (h / 2))) || point.y > (ymax + (h / 2)))
      return false;

    if (p1.x == p2.x)
      return ((point.x >= (p1.x - h / 2)) && (point.x <= (p1.x + h / 2)));

    if (p1.y == p2.y)
      return ((point.y >= (p1.y - h / 2)) && (point.y <= (p1.y + h / 2)));

    final double a = (p2.y - p1.y) / (p2.x - p1.x);
    final double b = (p1.y == ymin) ? ymin : ymax;

    final double y = (a * (point.x - xmin)) + b;

    return (point.y >= (y - h)) && (point.y <= (y + h));
  }

  public boolean hasNode(Node node) {
    return (entry.x == node) || (entry.y == node);
  }

  public Position relativeCenter() {
    final Position p1 = entry.x.relativeCenter();
    final Position p2 = entry.y.relativeCenter();

    final double xmin = Math.min(p1.x, p2.x);
    final double xmax = Math.max(p1.x, p2.x);

    final double ymin = Math.min(p1.y, p2.y);
    final double ymax = Math.max(p1.y, p2.y);

    return new Position((xmax + xmin) / 2, (ymax + ymin) / 2);
  }

  public Node getPartnerOfNode(Node node) {
    return (entry.x != node) ? entry.x : entry.y;
  }
}
