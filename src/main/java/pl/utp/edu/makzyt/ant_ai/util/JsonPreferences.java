package pl.utp.edu.makzyt.ant_ai.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import lombok.Data;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashMap;

@Data
public class JsonPreferences {

  public static final String PREFS_PATH
    = System.getProperty("user.home") + File.separator + ".ant_ai" + File.separator + "prefs.json";
  public final String path;
  private HashMap<String, Object> prefs;

  private static HashMap<String, Object> defaultPrefs() {
    final HashMap<String, Object> defaultPrefs = new HashMap<>();

    defaultPrefs.put("minPheromoneLevel", 0.5);
    defaultPrefs.put("maxPheromoneLevel", 2.0);
    defaultPrefs.put("evaporationFactor", 0.1);
    defaultPrefs.put("depositQuantity", 1.0);
    defaultPrefs.put("locale", "ENGLISH");
    defaultPrefs.put("antSpawnPower", 5);
    defaultPrefs.put("antSpeed", 3);
    defaultPrefs.put("slider.pheromoneFactor", 0.3);
    defaultPrefs.put("slider.heuristicFactor", 0.7);

    return defaultPrefs;
  }

  public void load()
    throws IOException {

    Gson gson = new Gson();
    BufferedReader br = buildBufferedReader();
    Type type = hashMapType();
    HashMap<String, Object> loadedPrefs = gson.fromJson(br, type);

    prefs = defaultPrefs();

    combineMaps(prefs, loadedPrefs);
  }

  private BufferedReader buildBufferedReader()
    throws IOException {

    BufferedReader br;

    try {
      br = new BufferedReader(new FileReader(path));
    } catch (FileNotFoundException e) {
      saveDefault();
      br = new BufferedReader(new FileReader(path));
    }

    return br;
  }

  private Type hashMapType() {
    return new TypeToken<HashMap<String, Object>>() {
    }.getType();
  }

  private void combineMaps(HashMap<String, Object> target,
                           HashMap<String, Object> source) {
    for (String key : source.keySet())
      target.put(key, source.get(key));
  }

  public void save()
    throws IOException {

    saveHashMap(prefs);
  }

  public void saveDefault()
    throws IOException {

    saveHashMap(defaultPrefs());
  }

  private void saveHashMap(HashMap prefs)
    throws IOException {

    File file = new File(path);

    if (!file.exists()) {
      file.getParentFile().mkdirs();
      file.createNewFile();
    }

    FileWriter writer = new FileWriter(file);

    Gson gson = new GsonBuilder().create();
    gson.toJson(prefs, writer);

    writer.close();
  }

  public Object get(String key) {
    return prefs.get(key);
  }

  public void put(String key, Object value) {
    prefs.put(key, value);
  }
}
