package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

@AllArgsConstructor
@Data
public final class InfoText
  implements Drawable {

  private String text;
  private Position position;

  public InfoText() {
    text = "";
    position = new Position(0, 0);
  }

  @Override
  public void draw(GraphicsContext gc) {
    gc.setFill(Color.web(MainWindow.config.valueOf("infoTextColor")));
    gc.fillText(text, position.x + 10, position.y + 10);
  }
}
