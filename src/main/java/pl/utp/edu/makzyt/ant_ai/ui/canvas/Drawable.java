package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {

  void draw(GraphicsContext gc);
}
