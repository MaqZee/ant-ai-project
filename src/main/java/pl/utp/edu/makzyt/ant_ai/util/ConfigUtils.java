package pl.utp.edu.makzyt.ant_ai.util;

public class ConfigUtils {

  public static String getConfigPrefix(String typeName) {
    String[] values = typeName.toLowerCase().split("_");

    for (int i = 1; i < values.length; i++) {
      StringBuilder builder = new StringBuilder(values[i]);
      builder.setCharAt(0, Character.toUpperCase(builder.charAt(0)));

      values[i] = builder.toString();
    }

    return String.join("", values);
  }
}
