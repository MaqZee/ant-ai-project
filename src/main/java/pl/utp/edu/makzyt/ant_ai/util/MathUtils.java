package pl.utp.edu.makzyt.ant_ai.util;

import java.util.Random;

public final class MathUtils {

  public static int ensureRange(int x, int min, int max) {
    return Math.max(Math.min(x, max), min);
  }

  public static double ensureRange(double x, double min, double max) {
    return Math.max(Math.min(x, max), min);
  }

  public static long ensureRange(long x, long min, long max) {
    return Math.max(Math.min(x, max), min);
  }

  public static float ensureRange(float x, float min, float max) {
    return Math.max(Math.min(x, max), min);
  }

  public static double randomDouble(double min, double max) {
    final Random random = new Random();
    return min + (max - min) * random.nextDouble();
  }
}
