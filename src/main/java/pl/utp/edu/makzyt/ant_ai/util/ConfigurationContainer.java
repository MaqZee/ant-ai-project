package pl.utp.edu.makzyt.ant_ai.util;

import pl.utp.edu.makzyt.ant_ai.ui.canvas.ConnectionEntry;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Node;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Position;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public final class ConfigurationContainer {
  public List<List<Node>> nodes = new ArrayList<>();
  public List<ConnectionEntry<Position, Position>> entries = new ArrayList<>();
  public HashMap<String, Object> preferences = new HashMap<>();
}
