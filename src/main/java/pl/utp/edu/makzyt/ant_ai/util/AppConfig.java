package pl.utp.edu.makzyt.ant_ai.util;

import java.util.ResourceBundle;

public class AppConfig {

  public final ResourceBundle bundle = ResourceBundle.getBundle("config");

  public String valueOf(String key) {
    return bundle.getString(key);
  }
}
