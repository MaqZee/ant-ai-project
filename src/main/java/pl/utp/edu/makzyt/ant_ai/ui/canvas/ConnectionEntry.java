package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
public class ConnectionEntry<T, U> {

  public T x;
  public U y;
}
