package pl.utp.edu.makzyt.ant_ai.ui;

import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeCanvas;
import pl.utp.edu.makzyt.ant_ai.ui.menus.LabeledIntSlider;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;
import pl.utp.edu.makzyt.ant_ai.util.ConfigurationManager;
import pl.utp.edu.makzyt.ant_ai.util.LocalizationBundle;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

@AllArgsConstructor
@Data
public final class MenuFactory {

  public static LocalizationBundle bundle;
  public static Stage stage;

  public static MenuBar buildTopMenuBar() {
    MenuBar menuBar = new MenuBar();

    menuBar.getMenus().addAll(
      buildFileMenu(),
      buildEditMenu());

    return menuBar;
  }

  private static Menu buildLocalizedMenu(String propertyKey) {
    return new Menu(MenuFactory.bundle.valueOf(propertyKey));
  }

  private static Menu buildFileMenu() {
    final Menu fileMenu = buildLocalizedMenu("menu.file");

    fileMenu.getItems().addAll(
      MenuItemFactory.buildNewMenuItem(),
      MenuItemFactory.buildOpenMenuItem(),
      MenuItemFactory.buildSaveMenuItem(),
      new SeparatorMenuItem(),
      MenuItemFactory.buildQuitMenuItem());

    return fileMenu;
  }

  private static Menu buildEditMenu() {
    final Menu editMenu = buildLocalizedMenu("menu.edit");

    editMenu.getItems().add(buildLocaleSubmenu());

    return editMenu;
  }

  private static Menu buildHelpMenu() {
    Menu helpMenu = buildLocalizedMenu("menu.help");

    helpMenu.getItems().addAll(
      MenuItemFactory.buildAboutMenuItem(),
      MenuItemFactory.buildFormulaMenuItem());

    return helpMenu;
  }

  private static Menu buildLocaleSubmenu() {
    final Menu localeMenu = buildLocalizedMenu("menu.edit.locale");

    final ToggleGroup toggleGroup = new ToggleGroup();

    RadioMenuItem englishItem = MenuItemFactory.buildLocaleEnglishMenuItem(toggleGroup);
    RadioMenuItem polishItem = MenuItemFactory.buildLocalePolishMenuItem(toggleGroup);

    switch ((String) MainWindow.prefs.get("locale")) {
      case "POLISH":
        polishItem.setSelected(true);
        break;
      default:
        englishItem.setSelected(true);
    }

    localeMenu.getItems().addAll(englishItem, polishItem);

    return localeMenu;
  }

  public static void showSaveDialog() {
    if (MainWindow.canvas.getStatus() == NodeCanvas.SimulationStatus.RUNNING)
      MainWindow.canvas.pauseSimulation();

    FileChooser chooser = new FileChooser();
    chooser.setTitle(MenuFactory.bundle.valueOf("dialog.save"));

    File file = chooser.showSaveDialog(stage);
    if (file == null)
      return;

    ConfigurationManager container = new ConfigurationManager(file);

    try {
      container.save();
    } catch (IOException e) {
      MainWindow.handleException(e);
    }
  }

  public static void showOpenDialog() {
    if (MainWindow.canvas.getStatus() == NodeCanvas.SimulationStatus.RUNNING)
      MainWindow.canvas.pauseSimulation();

    FileChooser chooser = new FileChooser();
    chooser.setTitle(MenuFactory.bundle.valueOf("dialog.open"));

    File file = chooser.showOpenDialog(stage);
    if (file == null)
      return;

    ConfigurationManager container = new ConfigurationManager(file);

    try {
      container.load();
      MainWindow.canvas.stopSimulation();
    } catch (IOException e) {
      MainWindow.handleException(e);
    }
  }

  public static void showNewDialog() {
    Stage stage = new Stage();

    AtomicInteger width = new AtomicInteger(10);
    AtomicInteger height = new AtomicInteger(10);

    LabeledIntSlider widthSlider = new LabeledIntSlider(MenuFactory.bundle.valueOf("dialog.new.width"),
      new Slider(5, 25, 10), width);
    LabeledIntSlider heightSlider = new LabeledIntSlider(MenuFactory.bundle.valueOf("dialog.new.height"),
      new Slider(5, 25, 10), height);

    Button acceptButton = new Button(MenuFactory.bundle.valueOf("dialog.button.apply"));
    acceptButton.setOnAction(e -> {
      MainWindow.canvas.makeNewGrid(width.intValue(), height.intValue());
      MainWindow.canvas.stopSimulation();
      stage.close();
    });

    Button cancelButton = new Button(MenuFactory.bundle.valueOf("dialog.button.cancel"));
    cancelButton.setOnAction(e -> stage.close());

    HBox hBox = new HBox(acceptButton, cancelButton);

    VBox vbox = new VBox(widthSlider, heightSlider, hBox);
    vbox.setPadding(new Insets(10));

    Scene scene = new Scene(vbox);

    stage.setScene(scene);
    stage.setTitle(MenuFactory.bundle.valueOf("dialog.new"));
    stage.initModality(Modality.WINDOW_MODAL);
    stage.initOwner(MenuFactory.stage);
    stage.show();
  }
}
