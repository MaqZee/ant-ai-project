package pl.utp.edu.makzyt.ant_ai.util;

import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationBundle {

  private final ResourceBundle bundle;

  public LocalizationBundle() {
    Locale locale = buildLocale((String) MainWindow.prefs.get("locale"));
    bundle = ResourceBundle.getBundle("strings", locale);
  }

  private Locale buildLocale(String name) {
    switch (name) {
      case "POLISH":
        return new Locale("pl", "PL");
      default:
        return new Locale("en", "EN");
    }
  }

  public String valueOf(String key) {
    return bundle.getString(key);
  }

  public void localizeAndReset(String localeName) {
    MainWindow.prefs.put("locale", localeName);
  }
}
