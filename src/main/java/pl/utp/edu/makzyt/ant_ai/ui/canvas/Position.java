package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.awt.*;
import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Position
  implements Serializable {

  public double x;
  public double y;

  public Position(Point point) {
    this(point.x, point.y);
  }

  public Position(Position position) {
    this(position.x, position.y);
  }
}
