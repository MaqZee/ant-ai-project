package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import pl.utp.edu.makzyt.ant_ai.ui.menus.context.ConnectionContextMenu;
import pl.utp.edu.makzyt.ant_ai.ui.menus.context.NodeContextMenu;


final public class EventHandlingMethods {

  public static boolean shiftPressed = false;

  protected static void handleMouseClicked(NodeCanvas canvas,
                                           MouseEvent event) {
    double x = event.getX();
    double y = event.getY();

    for (int i = 0; i < canvas.getNodes().size(); i++) {
      for (int j = 0; j < canvas.getNodes().get(i).size(); j++) {
        Node node = canvas.getNodes().get(i).get(j);

        if (node.inArea(new Position(x, y))) {
          buttonAction(event, canvas, node);
          return;
        }
      }
    }

    for (NodeConnection nc : canvas.getConnections()) {
      if (nc.inArea(new Position(x, y)))
        if (event.getButton() == MouseButton.SECONDARY) {
          popupMenuConnection(canvas, nc);

        }
    }

    canvas.getPhantomLine().setActive(false);
//    canvas.draw();
  }

  private static void buttonAction(MouseEvent event, NodeCanvas canvas, Node node) {
    switch (event.getButton()) {
      case PRIMARY:
        selectNode(node, canvas);
        break;
      case SECONDARY:
        popupMenuNode(canvas, node);
        break;
    }
  }

  private static void selectNode(Node node, NodeCanvas canvas) {
    if (node.getType() == Node.Type.NODE_INACTIVE)
      node.setType(Node.Type.NODE_ACTIVE);
    else {
      PhantomLine line = canvas.getPhantomLine();

      if (line.isActive()) {
        NodeConnection connection = line.connectNodes(node);

        addConnection(connection, canvas);

        line.setActive(false);
      } else if (shiftPressed) {
        line.setP1(node.relativeCenter());
        line.setP2(node.relativeCenter());
        line.setStartNode(node);
        line.setActive(true);
      } else {
        node.delete(canvas);
      }
    }

//    canvas.draw();
  }

  private static void popupMenuNode(NodeCanvas canvas, Node node) {
    canvas.hideContextMenu();

    canvas.setContextMenu(new NodeContextMenu(canvas, node));
    canvas.getContextMenu().init();
    canvas.getContextMenu().showAtMousePointer();
  }

  private static void popupMenuConnection(NodeCanvas canvas, NodeConnection connection) {
    canvas.hideContextMenu();

    canvas.setContextMenu(new ConnectionContextMenu(canvas, connection));
    canvas.getContextMenu().init();
    canvas.getContextMenu().showAtMousePointer();
  }

  protected static void handleMouseMoved(NodeCanvas canvas,
                                         MouseEvent event) {
    double x = event.getX();
    double y = event.getY();

    InfoText info = canvas.getInfoText();

    canvas.getInfoText().setText("");

    updateNodes(canvas, x, y, info);
    updateConnections(canvas, x, y, info);

    if (canvas.getPhantomLine().isActive()) {
      Position mousePos = new Position(x, y);

      canvas.getPhantomLine().setP2(new Position(mousePos));
    }
  }

  public static void addConnection(NodeConnection connection, NodeCanvas canvas) {
    if (connection == null)
      return;

    for (NodeConnection nc : canvas.getConnections()) {
      Node nc1 = nc.getEntry().x;
      Node nc2 = nc.getEntry().y;

      Node n1 = connection.getEntry().x;
      Node n2 = connection.getEntry().y;

      if (((n1 == nc1) && (n2 == nc2)) || ((n1 == nc2) && (n2 == nc1)))
        return;
    }

    canvas.getConnections().add(connection);
  }

  private static void updateNodes(NodeCanvas canvas, double x, double y, InfoText info) {
    for (int i = 0; i < canvas.getNodes().size(); i++) {
      for (int j = 0; j < canvas.getNodes().get(i).size(); j++) {
        Node node = canvas.getNodes().get(i).get(j);
        node.setSelected(false);

        if (node.inArea(new Position(x, y))) {

          if (node.getType() != Node.Type.NODE_INACTIVE)
            node.setSelected(true);

          info.setPosition(new Position(node.relativeCenter().x + 15, node.relativeCenter().y + 15));

          info.setText(String.format("[%.0f, %.0f]", node.getPosition().x, node.getPosition().y));

          return;
        }
      }
    }
  }

  private static void updateConnections(NodeCanvas canvas, double x, double y, InfoText info) {
    for (NodeConnection nc : canvas.getConnections()) {
      nc.setSelected(false);

      if (nc.inArea(new Position(x, y))) {
        nc.setSelected(true);
        nc.getEntry().x.setSelected(true);
        nc.getEntry().y.setSelected(true);

        info.setPosition(new Position(nc.relativeCenter().x + 15, nc.relativeCenter().y + 15));

        info.setText(String.format("[l=%.02f, ph=%.1f]", nc.length(), nc.getPheromoneLevel()));

        return;
      }
    }
  }

  public static void handleKeyPressed(KeyEvent event) {
    if (event.isShiftDown())
      shiftPressed = true;
  }

  public static void handleKeyReleased(KeyEvent event) {
    if (!event.isShiftDown())
      shiftPressed = false;
  }
}
