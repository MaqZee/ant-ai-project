package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.*;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;
import pl.utp.edu.makzyt.ant_ai.util.ConfigUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Data
public final class Node
  implements Drawable, Serializable {

  @NonNull
  private Position position;
  @NonNull
  private Type type;
  private boolean selected = false;

  protected Position relativePosition() {
    double pixelX = (position.x + 1) * Integer.parseInt(MainWindow.config.valueOf("nodeDistance"));

    double pixelY = (position.y + 1) * Integer.parseInt(MainWindow.config.valueOf("nodeDistance"));

    return new Position(pixelX - (radius() / 2), pixelY - (radius() / 2));
  }

  protected Position relativeCenter() {
    Position point = relativePosition();
    point.x += radius() / 2;
    point.y += radius() / 2;

    return point;
  }

  public boolean inArea(Position point) {
    Position relative = relativePosition();

    return (point.x >= relative.x) && (point.x <= (relative.x + radius()))
      && (point.y >= relative.y) && (point.y <= (relative.y + radius()));
  }

  public Color color() {
    String configKey = ConfigUtils.getConfigPrefix(type.name() + "_Color");
    String colorHex = MainWindow.config.valueOf(configKey);

    Color color = Color.web(colorHex);

    if (selected)
      color = ColorManipulator.brighten(color, 0.1);

    return color;
  }

  public double radius() {
    String configKey = ConfigUtils.getConfigPrefix(type.name() + "_Radius");
    double radius = Double.parseDouble(MainWindow.config.valueOf(configKey));

    if (selected)
      radius += 2;

    return radius;
  }

  public void draw(GraphicsContext gc) {
    if (MainWindow.controlPane != null) {
      if (type == Type.NODE_INACTIVE && !MainWindow.controlPane.getInactiveNodesCheckBox().getBox().isSelected())
        return;
    }

    double pixelX = (position.x + 1) * Integer.parseInt(MainWindow.config.valueOf("nodeDistance"));
    double pixelY = (position.y + 1) * Integer.parseInt(MainWindow.config.valueOf("nodeDistance"));

    gc.setFill(color());
    gc.fillOval(pixelX - radius() / 2, pixelY - radius() / 2,
      radius(), radius());
  }

  public void delete(NodeCanvas canvas) {
    for (Iterator<NodeConnection> iter = canvas.getConnections().listIterator(); iter.hasNext(); ) {
      NodeConnection nc = iter.next();
      ConnectionEntry<Node, Node> nodeEntry = nc.getEntry();

      if (nodeEntry.x == this || nodeEntry.y == this)
        iter.remove();
    }

    type = Type.NODE_INACTIVE;
  }

  public List<NodeConnection> connections(NodeCanvas canvas) {
    List<NodeConnection> connections = new ArrayList<>();

    for (NodeConnection nc : canvas.getConnections()) {
      if (nc.getEntry().y == this || nc.getEntry().x == this)
        connections.add(nc);
    }

    return connections;
  }

  public enum Type {
    NODE_INACTIVE,
    NODE_ACTIVE,
    NODE_SPAWNER,
    NODE_VISIT_POINT
  }
}
