package pl.utp.edu.makzyt.ant_ai.ui.menus.context;

import javafx.scene.control.ContextMenu;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeCanvas;

import java.awt.*;

@Data
public abstract class AppContextMenu extends ContextMenu {

  private NodeCanvas canvas;

  public abstract void init();

  public void showAtMousePointer() {
    show(canvas,
      MouseInfo.getPointerInfo().getLocation().x,
      MouseInfo.getPointerInfo().getLocation().y);
  }
}
