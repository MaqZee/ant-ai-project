package pl.utp.edu.makzyt.ant_ai.ui.menus;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import lombok.Data;

@Data
public class LabeledCheckBox extends BorderPane {

  private Label label;
  private CheckBox box;

  public LabeledCheckBox(String text) {
    label = new Label(text);
    box = new CheckBox();

    setLeft(label);
    setRight(box);
  }
}
