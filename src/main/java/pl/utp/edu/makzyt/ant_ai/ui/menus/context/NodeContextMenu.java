package pl.utp.edu.makzyt.ant_ai.ui.menus.context;

import javafx.scene.control.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.MenuFactory;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Ant;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Node;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeCanvas;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

import java.awt.*;
import java.util.ArrayList;

@AllArgsConstructor
@Data
public class NodeContextMenu extends AppContextMenu {

  private NodeCanvas canvas;
  private Node node;

  public void init() {
    addDeleteNodeMenuItem();
    addSetSpawnMenuItem();
    addSetVisitPointMenuItem();
  }

  private void addDeleteNodeMenuItem() {
    final MenuItem deleteNode
      = new MenuItem(MenuFactory.bundle.valueOf("menu.popup.delete"));
    deleteNode.setOnAction(actionEvent -> {
      if (node.getType() == Node.Type.NODE_SPAWNER)
        MainWindow.canvas.stopSimulation();

      node.delete(canvas);
      canvas.draw();
    });

    if (node.getType() == Node.Type.NODE_INACTIVE)
      deleteNode.setDisable(true);

    getItems().add(deleteNode);
  }

  private void addSetSpawnMenuItem() {
    final MenuItem setSpawnNode
      = new MenuItem(MenuFactory.bundle.valueOf("menu.popup.setSpawn"));
    setSpawnNode.setOnAction(actionEvent -> {
      node.setType(Node.Type.NODE_SPAWNER);
      Ant.shortestPath = new ArrayList<>();
      canvas.draw();
    });

    if (node.getType() == Node.Type.NODE_SPAWNER)
      setSpawnNode.setDisable(true);

    getItems().add(setSpawnNode);
  }

  private void addSetVisitPointMenuItem() {
    final MenuItem setVisitPointNode
      = new MenuItem(MenuFactory.bundle
      .valueOf("menu.popup.setVisitPoint"));
    setVisitPointNode.setOnAction(actionEvent -> {
      node.setType(Node.Type.NODE_VISIT_POINT);
      Ant.shortestPath = new ArrayList<>();
      canvas.draw();
    });

    if (node.getType() == Node.Type.NODE_VISIT_POINT)
      setVisitPointNode.setDisable(true);

    getItems().add(setVisitPointNode);
  }

  public void showAtMousePointer() {
    show(canvas,
      MouseInfo.getPointerInfo().getLocation().x,
      MouseInfo.getPointerInfo().getLocation().y);
  }
}
