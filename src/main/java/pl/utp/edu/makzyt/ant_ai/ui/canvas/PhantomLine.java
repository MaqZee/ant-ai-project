package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

@RequiredArgsConstructor
@Data
public final class PhantomLine
  implements Drawable {

  @NonNull
  private Position p1;
  @NonNull
  private Position p2;
  private Node startNode;
  private boolean active = false;

  @Override
  public void draw(GraphicsContext gc) {
    gc.setLineWidth(Integer.parseInt(MainWindow.config.valueOf("connectionThickness")));
    gc.setStroke(Color.web(MainWindow.config.valueOf("phantomColor")));
    gc.strokeLine(p1.x, p1.y, p2.x, p2.y);
  }

  public NodeConnection connectNodes(Node endNode) {
    if (startNode == endNode || startNode == null || endNode == null)
      return null;

    return new NodeConnection(startNode, endNode);
  }
}
