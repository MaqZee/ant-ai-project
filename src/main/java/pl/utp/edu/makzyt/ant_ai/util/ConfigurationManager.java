package pl.utp.edu.makzyt.ant_ai.util;

import lombok.Data;
import org.codehaus.jackson.map.ObjectMapper;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.*;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow.canvas;
import static pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow.prefs;

@Data
public final class ConfigurationManager {

  private final ObjectMapper mapper = new ObjectMapper();
  private ConfigurationContainer container;
  private File file;

  public ConfigurationManager(File file) {
    MainWindow.canvas.pauseSimulation();
    container = new ConfigurationContainer();
    container.nodes = canvas.getNodes();

    makeEntriesFromConnections(canvas.getConnections());

    container.preferences = prefs.getPrefs();
    this.file = file;
  }

  private void makeEntriesFromConnections(List<NodeConnection> connections) {
    for (NodeConnection connection : connections) {
      container.entries.add(new ConnectionEntry<>(
        connection.getEntry().x.getPosition(),
        connection.getEntry().y.getPosition()));
    }
  }

  private void makeConnectionsFromEntries(List<ConnectionEntry<Position, Position>> entries) {
    canvas.setConnections(new ArrayList<>());

    List<List<Node>> nodes = canvas.getNodes();

    for (ConnectionEntry<Position, Position> entry : entries) {
      Position pos1 = entry.x;
      Position pos2 = entry.y;

      Node node1 = nodes.get((int) pos1.y).get((int) pos1.x);
      Node node2 = nodes.get((int) pos2.y).get((int) pos2.x);

      EventHandlingMethods.addConnection(new NodeConnection(node1, node2), canvas);
    }
  }

  public void load()
    throws IOException {

    ConfigurationContainer container = mapper.readValue(file, ConfigurationContainer.class);

    canvas.setNodes(container.nodes);
    makeConnectionsFromEntries(container.entries);
    MainWindow.prefs.setPrefs(container.preferences);
  }

  public void save()
    throws IOException {

    if (!file.exists()) {
      file.getParentFile().mkdirs();
      file.createNewFile();
    }

    mapper.writeValue(file, container);
  }
}
