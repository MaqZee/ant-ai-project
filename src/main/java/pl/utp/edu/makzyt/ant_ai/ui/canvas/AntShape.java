package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.canvas.GraphicsContext;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

@RequiredArgsConstructor
@Data
public class AntShape
  implements Drawable {

  private static final double radius = Double.parseDouble(MainWindow.config.valueOf("antSize"));
  @NonNull
  private Position position;

  @Override
  public void draw(GraphicsContext gc) {
    gc.fillOval(position.x - radius, position.y - radius, radius * 2, radius * 2);
    gc.fillPolygon(
      new double[]{
        position.x - radius,
        position.x,
        position.x + radius
      },
      new double[]{
        position.y,
        position.y - (radius * 2),
        position.y
      },
      3);
    gc.restore();
  }
}
