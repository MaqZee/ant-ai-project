package pl.utp.edu.makzyt.ant_ai.ui.canvas;

import javafx.scene.paint.Color;
import pl.utp.edu.makzyt.ant_ai.util.MathUtils;

public final class ColorManipulator {

  public static Color brighten(Color color, double alpha) {
    double r = MathUtils.ensureRange(color.getRed() + alpha, 0, 1);
    double g = MathUtils.ensureRange(color.getGreen() + alpha, 0, 1);
    double b = MathUtils.ensureRange(color.getBlue() + alpha, 0, 1);

    return new Color(r, g, b, color.getOpacity());
  }
}
