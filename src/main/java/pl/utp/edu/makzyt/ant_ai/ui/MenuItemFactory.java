package pl.utp.edu.makzyt.ant_ai.ui;

import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.ToggleGroup;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

public class MenuItemFactory {

  private static MenuItem buildLocalizedMenuItem(String propertyKey) {
    return new MenuItem(MenuFactory.bundle.valueOf(propertyKey));
  }

  private static RadioMenuItem buildLocalizedRadioMenuItem(String propertyKey,
                                                           ToggleGroup group) {
    RadioMenuItem item = new RadioMenuItem(MenuFactory.bundle.valueOf(propertyKey));
    item.setToggleGroup(group);

    return item;
  }

  // TODO events
  protected static MenuItem buildNewMenuItem() {
    MenuItem newMenuItem = buildLocalizedMenuItem("menu.file.new");

    newMenuItem.setOnAction(event -> MenuFactory.showNewDialog());

    return newMenuItem;
  }

  // TODO events
  protected static MenuItem buildOpenMenuItem() {
    MenuItem openMenuItem = buildLocalizedMenuItem("menu.file.open");

    openMenuItem.setOnAction(event -> MenuFactory.showOpenDialog());

    return openMenuItem;
  }

  // TODO events
  protected static MenuItem buildSaveMenuItem() {
    MenuItem saveMenuItem = buildLocalizedMenuItem("menu.file.save");

    saveMenuItem.setOnAction(event -> MenuFactory.showSaveDialog());

    return saveMenuItem;
  }

  protected static MenuItem buildQuitMenuItem() {
    MenuItem quitMenuItem = buildLocalizedMenuItem("menu.file.quit");

    quitMenuItem.setOnAction(event -> MainWindow.saveAndClose());

    return quitMenuItem;
  }

  protected static RadioMenuItem buildLocaleEnglishMenuItem(ToggleGroup group) {
    RadioMenuItem localeEnglishMenuItem = buildLocalizedRadioMenuItem("menu.edit.locale.english", group);

    localeEnglishMenuItem.setToggleGroup(group);
    localeEnglishMenuItem.setOnAction(actionEvent ->
      MenuFactory.bundle.localizeAndReset("ENGLISH"));

    return localeEnglishMenuItem;
  }

  protected static RadioMenuItem buildLocalePolishMenuItem(ToggleGroup group) {
    RadioMenuItem localeEnglishMenuItem = buildLocalizedRadioMenuItem("menu.edit.locale.polish", group);

    localeEnglishMenuItem.setOnAction(actionEvent ->
      MenuFactory.bundle.localizeAndReset("POLISH"));

    return localeEnglishMenuItem;
  }

  // TODO events
  protected static MenuItem buildAboutMenuItem() {
    return buildLocalizedMenuItem("menu.help.about");
  }

  // TODO events
  protected static MenuItem buildFormulaMenuItem() {
    return buildLocalizedMenuItem("menu.help.formula");
  }
}
