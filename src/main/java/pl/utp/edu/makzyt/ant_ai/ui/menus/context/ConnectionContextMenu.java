package pl.utp.edu.makzyt.ant_ai.ui.menus.context;

import javafx.scene.control.MenuItem;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.MenuFactory;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.Ant;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeCanvas;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeConnection;

import java.awt.*;
import java.util.ArrayList;

@AllArgsConstructor
@Data
public class ConnectionContextMenu extends AppContextMenu {

  private NodeCanvas canvas;
  private NodeConnection connection;

  public void init() {
    addDeleteConnectionMenuItem();
  }

  private void addDeleteConnectionMenuItem() {
    final MenuItem deleteConnection = new MenuItem(MenuFactory.bundle.valueOf("menu.popup.deleteConnection"));
    deleteConnection.setOnAction(actionEvent -> {
      if (Ant.shortestPath.contains(connection)) {
        Ant.shortestPath = new ArrayList<>();
      }

      canvas.getConnections().remove(connection);
      canvas.draw();
    });

    getItems().add(deleteConnection);
  }


  public void showAtMousePointer() {
    show(canvas,
      MouseInfo.getPointerInfo().getLocation().x,
      MouseInfo.getPointerInfo().getLocation().y);
  }
}
