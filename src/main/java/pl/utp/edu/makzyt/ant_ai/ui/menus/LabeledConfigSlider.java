package pl.utp.edu.makzyt.ant_ai.ui.menus;

import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.window.MainWindow;

@Data
public class LabeledConfigSlider extends VBox {

  private Label label;
  private Label countLabel;
  private Slider slider;

  public LabeledConfigSlider(String text, Slider slider, String configName) {
    label = new Label(text);
    countLabel = new Label();
    this.slider = slider;
    slider.valueProperty().addListener(event -> {
      MainWindow.prefs.put(configName, slider.getValue());
      countLabel.setText(sliderValueString());
    });

    countLabel = new Label(sliderValueString());

    getChildren().add(label);
    getChildren().add(new HBox(slider, countLabel));
  }

  public String sliderValueString() {
    return String.format("%.1f", slider.getValue());
  }
}
