package pl.utp.edu.makzyt.ant_ai.ui.window;

import javafx.application.Application;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import lombok.Data;
import pl.utp.edu.makzyt.ant_ai.ui.MenuFactory;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.EventHandlingMethods;
import pl.utp.edu.makzyt.ant_ai.ui.canvas.NodeCanvas;
import pl.utp.edu.makzyt.ant_ai.ui.menus.ControlPane;
import pl.utp.edu.makzyt.ant_ai.util.AppConfig;
import pl.utp.edu.makzyt.ant_ai.util.JsonPreferences;
import pl.utp.edu.makzyt.ant_ai.util.LocalizationBundle;

import java.io.IOException;

@Data
public class MainWindow
  extends Application {

  public final static AppConfig config = new AppConfig();
  public static JsonPreferences prefs = new JsonPreferences(JsonPreferences.PREFS_PATH);
  public static NodeCanvas canvas = new NodeCanvas();
  public static ControlPane controlPane;

  public static void main(String[] args) {
    launch(args);
  }

  private static void loadPreferences()
    throws IOException {

    prefs.load();
  }

  private static BorderPane prepareMainPane() {
    final BorderPane borderPane = new BorderPane();
    borderPane.setTop(MenuFactory.buildTopMenuBar());
    borderPane.setCenter(prepareMainSplitPane());

    return borderPane;
  }

  private static SplitPane prepareMainSplitPane() {
    final SplitPane splitPane = new SplitPane();
    splitPane.orientationProperty().set(Orientation.HORIZONTAL);

    splitPane.getItems().addAll(controlPane, canvas);

    return splitPane;
  }

  public static void saveAndClose() {
    prefs.put("minPheromoneLevel", controlPane.getMinPheromoneLevelSlider().getSlider().getValue());
    prefs.put("maxPheromoneLevel", controlPane.getMaxPheromoneLevelSlider().getSlider().getValue());
    prefs.put("evaporationFactor", controlPane.getEvaporationSlider().getSlider().getValue());
    prefs.put("depositQuantity", controlPane.getPheromoneDepositSlider().getSlider().getValue());

    try {
      prefs.save();
    } catch (IOException e) {
      handleException(e);
    }

    MenuFactory.stage.close();
  }

  public static void handleException(Exception e) {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle(MenuFactory.bundle.valueOf("dialog.error"));
    alert.setContentText(e.getMessage());
  }

  // TODO handle exceptions
  @Override
  public void start(Stage primaryStage)
    throws IOException {

    loadPreferences();

    MenuFactory.stage = primaryStage;
    MenuFactory.bundle = new LocalizationBundle();

    controlPane = new ControlPane();

    Scene scene = new Scene(prepareMainPane(), 640, 480);
    scene.setOnKeyPressed(EventHandlingMethods::handleKeyPressed);
    scene.setOnKeyReleased(EventHandlingMethods::handleKeyReleased);

    primaryStage.setTitle(MenuFactory.bundle.valueOf("window.title"));
    primaryStage.setScene(scene);
    primaryStage.setOnCloseRequest(event -> saveAndClose());
    primaryStage.show();
  }
}
